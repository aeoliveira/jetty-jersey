package br.com.jetty.resource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.jetty.TestService;

@Path("msg")
public class MyMessage {
	
	@Inject
	TestService testService;

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getMessage() {
		
		testService.retornaMsg();

		return "My message\n";
	}
}
