package br.com.jetty;

import java.io.IOException;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.jboss.weld.environment.servlet.Listener;

public class Main {

    public static void main(String[] args) throws Exception {
    	
    	//http://localhost:8080/exchange/v1/msg     --URL inicial
    	
    	
    	Server server = new Server(8080);
        ServletContextHandler context = configureServletContext(server);

        configureCdi(context);
        ResourceConfig jerseyConfig = configureJersey();
        configureServlet(context, jerseyConfig);

        try {
            server.start();
            // server.dumpStdErr();
            server.join();
        } finally {
            server.destroy();
        }    	

    }
    
    private static ServletContextHandler configureServletContext(Server server) throws IOException {
        ServletContextHandler context = new ServletContextHandler();
        context.setContextPath("/exchange");

        server.setHandler(context);
        return context;
    }

    private static void configureCdi(ServletContextHandler context) {
        Listener listener = new Listener();
        context.addEventListener(listener);
        
    }

    private static ResourceConfig configureJersey() {
        ResourceConfig config = new ResourceConfig();
        config.packages("br.com.jetty.resource");
        return config;
    }

    private static void configureServlet(ServletContextHandler context, ResourceConfig jerseyConfig) {
        ServletHolder servlet = new ServletHolder(new ServletContainer(jerseyConfig));
        context.addServlet(servlet, "/v1/*");
    }    
}